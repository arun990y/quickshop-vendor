import 'dart:io';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import '../providers/auth.dart';

class ProductImage extends StatefulWidget {
  final Function getImageUrl;
  String imageUrl;
  final String userId;
  ProductImage(
    this.getImageUrl,
    this.imageUrl,
    this.userId,
  );
  @override
  _ProductImageState createState() => _ProductImageState();
}

class _ProductImageState extends State<ProductImage> {
  var _pickedImage;

  void _imgFromCamera() async {
    final picker = ImagePicker();

    final pickedImage = await picker.getImage(
      source: ImageSource.camera,
    );
    if (pickedImage == null) {
      return;
    }
    final pickedImageFile = File(pickedImage.path);
    setState(() {
      _pickedImage = pickedImageFile;
    });

    final ref = FirebaseStorage.instance
        .ref()
        .child('product_image')
        .child('productImage_' + widget.userId + '.jpg');

    await ref.putFile(pickedImageFile);

    final imageUrl = await ref.getDownloadURL();

    widget.getImageUrl(imageUrl);
  }

  _imgFromGallery() async {
    final picker = ImagePicker();

    final pickedImage = await picker.getImage(
      source: ImageSource.gallery,
    );
    if (pickedImage == null) {
      return;
    }
    final pickedImageFile = File(pickedImage.path);
    setState(() {
      _pickedImage = pickedImageFile;
    });

    final ref = FirebaseStorage.instance
        .ref()
        .child('product_image')
        .child('productImage_' + widget.userId + '.jpg');
    await ref.putFile(pickedImageFile);

    final imageUrl = await ref.getDownloadURL();

    widget.getImageUrl(imageUrl);
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        InkWell(
          onTap: () => _showPicker(context),
          child: Container(
              width: double.infinity,
              height: width * 0.55,
              margin: EdgeInsets.all(5),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: _pickedImage != null || widget.imageUrl != ""
                    ? Border.all(
                        width: 2,
                        color: Colors.black54,
                      )
                    : null,
                borderRadius: BorderRadius.circular(10),
              ),
              child: _pickedImage != null || widget.imageUrl != ""
                  ? SizedBox.expand(
                      child: FittedBox(
                        child: widget.imageUrl == ""
                            ? ClipRRect(
                                borderRadius: BorderRadius.circular(3),
                                child: Image.file(
                                  _pickedImage,
                                ),
                                clipBehavior: Clip.hardEdge,
                              )
                            : ClipRRect(
                                borderRadius: BorderRadius.circular(3),
                                child: Image.network(widget.imageUrl),
                                clipBehavior: Clip.hardEdge,
                              ),
                        fit: BoxFit.fill,
                      ),
                    )
                  : DottedBorder(
                      radius: Radius.circular(10),
                      borderType: BorderType.RRect,
                      dashPattern: [3, 8],
                      child: Container(
                        alignment: Alignment.center,
                        child: IconButton(
                          icon: SvgPicture.asset(
                            'assets/svgIcons/photo-camera.svg',
                            color: Colors.black54,
                          ),
                          iconSize: 100,
                          onPressed: () => _showPicker(context),
                        ),
                      ),
                    )),
        ),
      ],
    );
  }
}
