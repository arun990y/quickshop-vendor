import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/product.dart';
import '../screens/addProduct.dart';
import '../screens/product_description.dart';

class ProductItem extends StatefulWidget {
  final ProductModel product;
  final String userId;
  final String category;
  final Function removeProduct;
  final Function updateProduct;
  ProductItem(
    this.product,
    this.userId,
    this.category,
    this.removeProduct,
    this.updateProduct,
  );

  @override
  _ProductItemState createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {
  ProductModel product;

  @override
  void initState() {
    super.initState();
    product = widget.product;
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return InkWell(
      onTap: () {
        // Navigator.of(context).pushNamed(
        //   ProductDescriptionScreen.routeName,
        //   arguments: product,
        // );
      },
      child: Container(
        margin: EdgeInsets.all(width * 0.02),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
            blurRadius: 10,
            color: Colors.grey[300],
          ),
        ]),
        child: Card(
          elevation: 0,
          shadowColor: Colors.grey,
          child: Container(
            padding: EdgeInsets.all(width * 0.04),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(shape: BoxShape.circle),
                        child: CircleAvatar(
                          radius: 40,
                          backgroundImage: product.imageUrl != null
                              ? NetworkImage(product.imageUrl)
                              : null,
                        ),
                      ),
                      SizedBox(width: width * 0.03),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            constraints: BoxConstraints(maxHeight: width * 0.3),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(
                                '${product.productName}',
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 22,
                                ),
                              ),
                            ),
                          ),
                          Container(
                            constraints: BoxConstraints(maxHeight: width * 0.3),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(
                                '${product.category}',
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 17,
                                ),
                              ),
                            ),
                          ),
                          Text(
                            '\u20b9 ${product.price} per ${product.unit}',
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 16,
                            ),
                          ),
                          // Text(
                          //   '\u20b9 ${product.price.toStringAsFixed(2)}',
                          //   style: TextStyle(
                          //     fontWeight: FontWeight.w500,
                          //     fontSize: 16,
                          //   ),
                          // ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  width: width * 0.27,
                  child: Row(
                    children: [
                      IconButton(
                          icon: Icon(
                            Icons.edit,
                            color: Colors.blue,
                            size: 20,
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => AddProductScreen(
                                  userId: widget.userId,
                                  product: product,
                                ),
                              ),
                            ).then((value) {
                              if (value != null) {
                                setState(() {
                                  product = value;
                                });
                                widget.updateProduct(product.id, value);
                              }
                              if (product.category != widget.category)
                                widget.removeProduct(product.id);
                            });
                          },
                          color: Colors.black),
                      IconButton(
                        icon: Icon(
                          Icons.delete,
                          size: 20,
                        ),
                        onPressed: () {
                          showDialog(
                              context: context,
                              child: AlertDialog(
                                title: Text(
                                  'Deleting Product!',
                                  style: TextStyle(fontSize: 22),
                                ),
                                content: Text(
                                    'Are you sure you want to delete this product?',
                                    style: TextStyle(fontSize: 20)),
                                actions: [
                                  FlatButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Text('No',
                                        style: TextStyle(fontSize: 18)),
                                  ),
                                  FlatButton(
                                    onPressed: () {
                                      Provider.of<Product>(context,
                                              listen: false)
                                          .deleteProduct(
                                        widget.product.id,
                                        widget.userId,
                                      );
                                      widget.removeProduct(product.id);
                                      Navigator.of(context).pop();
                                    },
                                    child: Text(
                                      'Yes',
                                      style: TextStyle(fontSize: 18),
                                    ),
                                  )
                                ],
                              ));
                        },
                        color: Theme.of(context).errorColor,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
