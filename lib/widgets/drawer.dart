import 'dart:convert';

import 'package:QuickshopVendors/screens/auth_screen.dart';
import 'package:QuickshopVendors/screens/startUp_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import '../providers/auth.dart';

class MainDrawer extends StatefulWidget {
  @override
  _MainDrawerState createState() => _MainDrawerState();
}

class _MainDrawerState extends State<MainDrawer> {
  final LocalStorage storage = new LocalStorage('QUICKSHOP_V');
  var userDetails;
  bool _isLoading = true;

  Widget buildListTile(
      String title, String icon, Function tapHandler, double width) {
    return GestureDetector(
      onTap: tapHandler,
      child: Container(
        height: width * 0.1,
        padding: EdgeInsets.only(left: width * 0.045),
        margin: EdgeInsets.only(bottom: width * 0.03),
        alignment: Alignment.center,
        child: Row(
          children: [
            SvgPicture.asset(
              icon,
              height: 22,
              width: 22,
            ),
            SizedBox(width: width * 0.04),
            Text(
              title,
              style: TextStyle(
                fontSize: 20,
                color: Color.fromRGBO(40, 40, 40, 1),
                fontWeight: FontWeight.w500,
              ),
            ),
          ],
        ),
      ),
    );
  }

  fetchData() async {
    await storage.ready;
    userDetails = json.decode(storage.getItem('userDetails'));
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Drawer(
        child: _isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(
                      bottom: width * 0.04,
                      top: width * 0.04,
                    ),
                    child: ListTile(
                      leading: CircleAvatar(
                        radius: 30,
                        backgroundColor: Colors.grey[300],
                        backgroundImage: userDetails['shopImageUrl'] == null
                            ? AssetImage('assets/images/6.jpg')
                            : NetworkImage(userDetails['shopImageUrl']),
                      ),
                      title: Text(
                        userDetails['ownerName'] == null
                            ? ""
                            : userDetails['ownerName'],
                        style: TextStyle(
                          fontSize: 20,
                          color: Color.fromRGBO(40, 40, 40, 1),
                        ),
                      ),
                      subtitle: Text(
                        userDetails['email'] == null
                            ? ""
                            : userDetails['email'],
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                  buildListTile(
                    'Home',
                    'assets/svgIcons/home.svg',
                    () {
                      Navigator.of(context).pushNamed('/');
                    },
                    width,
                  ),
                  buildListTile(
                    'My Orders',
                    'assets/svgIcons/order.svg',
                    () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => StartUpPage(
                            index: 1,
                          ),
                        ),
                      );
                    },
                    width,
                  ),
                  buildListTile(
                    'Logout',
                    'assets/svgIcons/logout.svg',
                    () async {
                      Navigator.of(context).pop();
                      await storage.deleteItem('userDetails');
                      Provider.of<Auth>(context, listen: false).logout();
                      Navigator.of(context)
                          .pushReplacementNamed(AuthScreen.routeName);
                    },
                    width,
                  ),
                ],
              ),
      ),
    );
  }
}
