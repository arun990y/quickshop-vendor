import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';

import '../providers/auth.dart';

class ShopImage extends StatefulWidget {
  final Function getimageUrl;
  final String userId;
  ShopImage(this.getimageUrl, this.userId);

  @override
  _ShopImageState createState() => _ShopImageState();
}

class _ShopImageState extends State<ShopImage> {
  File _pickedImage;

  void _imgFromCamera() async {
    final picker = ImagePicker();

    final pickedImage = await picker.getImage(
      source: ImageSource.camera,
    );
    if (pickedImage == null) {
      return;
    }
    final pickedImageFile = File(pickedImage.path);
    setState(() {
      _pickedImage = pickedImageFile;
    });

    final ref = FirebaseStorage.instance
        .ref()
        .child('shop_image')
        .child('shopImage_' + widget.userId + '.jpg');

    await ref.putFile(pickedImageFile);

    final imageUrl = await ref.getDownloadURL();

    widget.getimageUrl(imageUrl);
  }

  _imgFromGallery() async {
    final picker = ImagePicker();

    final pickedImage = await picker.getImage(
      source: ImageSource.gallery,
    );
    if (pickedImage == null) {
      return;
    }
    final pickedImageFile = File(pickedImage.path);
    setState(() {
      _pickedImage = pickedImageFile;
    });

    final ref = FirebaseStorage.instance
        .ref()
        .child('shop_image')
        .child('shopImage_' + widget.userId + '.jpg');

    await ref.putFile(pickedImageFile);

    final imageUrl = await ref.getDownloadURL();

    widget.getimageUrl(imageUrl);
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          InkWell(
            onTap: () => _showPicker(context),
            child: Container(
              height: width * 0.3,
              width: width * 0.28,
              alignment: Alignment.center,
              //  margin: EdgeInsets.only(left: 8),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 2,
                  color: Colors.black54,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
              child: _pickedImage != null
                  ? SizedBox.expand(
                      child: FittedBox(
                        child: Image.file(
                          _pickedImage,
                        ),
                        fit: BoxFit.contain,
                      ),
                    )
                  : Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width * 0.007,
                      ),
                      child: Text(
                        'Pick Shop image',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
            ),
          )
        ]);
  }
}
