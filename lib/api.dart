// var domain = "http://192.168.0.116:5000";
// var domain = "https://quick-shop12.herokuapp.com";
var domain = "http://3.109.6.67:5000";

var endPoint = {
  'register': "/api/v1/auth/register",
  "login": "/api/v1/auth/login",
  "location": "/api/v1/shop/location/",
  "nearShop": "/api/v1/shop/radius/",
  "insertShop": "/api/v1/shop/createShop",
  "insertProduct": '/api/v1/product/insert/',
  "fetchProduct": '/api/v1/product/',
  "deleteProduct": '/api/v1/product/delete/',
  "updateProduct": '/api/v1/product/update/',
  'fetchUser': '/api/v1/shop/fetchUser',
  'updateUser': '/api/v1/shop/updateUser',

  //ORDERS
  'fetchVendorOrder': '/api/v1/order/fetchVendorOrder',
  'updateOrder': '/api/v1/order/updateOrder',
};
