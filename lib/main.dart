import 'package:QuickshopVendors/providers/orders.dart';
import 'package:QuickshopVendors/screens/auth_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import './screens/businessDetail.dart';
import './providers/auth.dart';
import './providers/location.dart';
import './screens/map_screen.dart';
import './screens/user_location.dart';
import './screens/welcome_screen.dart';
import 'screens/addProduct.dart';
import './providers/product.dart';
import './screens/startUp_screen.dart';
import './screens/splash.dart';
import './screens/order_screen.dart';
import 'screens/listOfCategory.dart';
import './screens/product_description.dart';
import './helper/custom_route.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (context) => Auth(),
          ),
          ChangeNotifierProxyProvider<Auth, Location>(
            create: null,
            update: (ctx, auth, previousLocation) => Location(
              auth.userId,
            ),
          ),
          ChangeNotifierProxyProvider<Auth, Product>(
            create: null,
            update: (context, auth, _) => Product(auth.userId),
          ),
          ChangeNotifierProvider(
            create: (context) => Order(),
          ),
        ],
        child: Consumer<Auth>(
          builder: (ctx, authData, _) => MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'QuickShop-Vendors',
            theme: ThemeData(
              primaryColor: Colors.blue,
              fontFamily: 'MarkaziText',
              backgroundColor: Colors.white,
              pageTransitionsTheme: PageTransitionsTheme(builders: {
                TargetPlatform.android: CustomPageTransitionBuilder(),
                TargetPlatform.iOS: CustomPageTransitionBuilder(),
              }),
              appBarTheme: AppBarTheme(
                textTheme: ThemeData.light().textTheme.copyWith(
                      headline1: TextStyle(
                        fontSize: 23,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'DancingScript',
                        color: Colors.white,
                      ),
                    ),
              ),
            ),
            home: authData.auth
                ? StartUpPage()
                : FutureBuilder(
                    future: authData.tryAutoLogin(),
                    builder: (ctx, dataSnapshot) =>
                        dataSnapshot.connectionState == ConnectionState.waiting
                            ? SplashScreen()
                            : AuthScreen(),
                  ),
            routes: {
              AuthScreen.routeName: (ctx) => AuthScreen(),
              UserLocation.routeName: (ctx) => UserLocation(),
              MapScreen.routeName: (ctx) => MapScreen(),
              BusinessDetail.routeName: (ctx) => BusinessDetail(),
              WelcomeScreen.routeName: (ctx) => WelcomeScreen(),
              AddProductScreen.routeName: (ctx) => AddProductScreen(),
              StartUpPage.routeName: (ctx) => StartUpPage(),
              ListOfCategory.routeName: (ctx) => ListOfCategory(),
              ProductDescriptionScreen.routeName: (ctx) =>
                  ProductDescriptionScreen(),
            },
          ),
        ));
  }
}
