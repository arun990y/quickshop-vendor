import 'package:QuickshopVendors/providers/product.dart';
import 'package:QuickshopVendors/widgets/product_item.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListOfProduct extends StatefulWidget {
  final List<ProductModel> products;
  final String category;
  final String userId;
  ListOfProduct({
    this.products,
    this.userId,
    this.category,
  });
  @override
  _ListOfProductState createState() => _ListOfProductState();
}

class _ListOfProductState extends State<ListOfProduct> {
  // List<ProductModel> products;

  @override
  void initState() {
    super.initState();
  }

  removeProduct(String prodId) {
    setState(() {
      widget.products.removeWhere((prod) => prod.id == prodId);
    });
  }

  updateProduct(String prodId, product) {
    setState(() {
      widget.products.forEach((prod) {
        if (prod.id == prodId) {
          prod = product;
        }
      });
    });
  }

  Future<bool> _willPopCallback() async {
    Navigator.of(context).pop(true);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text(
              'List of Products',
              style: Theme.of(context).appBarTheme.textTheme.headline1,
            ),
          ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                child: widget.products.length == 0
                    ? Center(
                        child: Padding(
                          padding: EdgeInsets.only(
                            top: width * 0.04,
                            left: width * 0.02,
                            bottom: width * 0.02,
                            right: width * 0.02,
                          ),
                          child: Text(
                            'No products found.Add some products!',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 22,
                            ),
                          ),
                        ),
                      )
                    : ListView.builder(
                        itemBuilder: (ctx, i) => ProductItem(
                            widget.products[i],
                            widget.userId,
                            widget.category,
                            removeProduct,
                            updateProduct),
                        itemCount: widget.products.length,
                      ),
              ),
              
            ],
          ),
        ),
      ),
    );
  }
}
