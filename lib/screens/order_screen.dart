import 'dart:convert';
import 'package:QuickshopVendors/providers/orders.dart';
import 'package:QuickshopVendors/screens/orderDetails.dart';
import 'package:QuickshopVendors/screens/orderStatus.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:localstorage/localstorage.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class OrderScreen extends StatefulWidget {
  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen> {
  List<OrderModel> orders = [];
  final LocalStorage storage = new LocalStorage('QUICKSHOP_V');
  var _isLoading = true;
  Map<String, dynamic> userDetails;
  int deliveryCharge = 30;

  @override
  void initState() {
    super.initState();
    fetchOrder();
  }

  fetchOrder() async {
    await storage.ready;
    userDetails = json.decode(storage.getItem('userDetails'));
    final List<OrderModel> loadedOrder =
        await Provider.of<Order>(context, listen: false)
            .fetchVendorOrder(userDetails['userId']);
    if (loadedOrder != null) {
      orders = List.from(loadedOrder);
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        body: _isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Container(
                height: height,
                width: width,
                margin: EdgeInsets.symmetric(
                  vertical: width * 0.03,
                  horizontal: width * 0.04,
                ),
                child: orders.length == 0
                    ? SingleChildScrollView(
                        child: Container(
                          height: height * 0.7,
                          alignment: Alignment.center,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            // crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Align(
                                child: SvgPicture.asset(
                                  'assets/svgIcons/order.svg',
                                  height: height * 0.15,
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(top: width * 0.03),
                                child: Text(
                                  'Your basket is empty',
                                  style: TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    : SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Total Order ( ${orders.length} ):',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            ...orders.map((order) {
                              return InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          OrderStatus(order: order),
                                    ),
                                  ).then((value) {
                                    if (value) {
                                      setState(() {
                                        order.delivered = true;
                                        order.updatedAt = DateTime.now();
                                      });
                                    }
                                  });
                                },
                                child: Container(
                                  margin: EdgeInsets.only(
                                    top: width * 0.02,
                                  ),
                                  decoration: BoxDecoration(boxShadow: [
                                    BoxShadow(
                                      blurRadius: 10,
                                      color: Colors.grey[300],
                                    ),
                                  ]),
                                  child: Card(
                                    elevation: 0,
                                    shadowColor: Colors.grey,
                                    child: Container(
                                      padding: EdgeInsets.symmetric(
                                        vertical: width * 0.04,
                                        horizontal: width * 0.02,
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            child: Row(
                                              children: [
                                                Container(
                                                  child: CircleAvatar(
                                                    radius: 40,
                                                    backgroundColor:
                                                        Colors.black26,
                                                    backgroundImage:
                                                        order.shopImage != null
                                                            ? NetworkImage(
                                                                order.shopImage)
                                                            : null,
                                                  ),
                                                ),
                                                SizedBox(width: width * 0.054),
                                                Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Container(
                                                      constraints:
                                                          BoxConstraints(
                                                              maxWidth:
                                                                  width * 0.5),
                                                      child: Text(
                                                        'Ordered by ${order.customerName}',
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 22,
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      constraints:
                                                          BoxConstraints(
                                                              maxWidth:
                                                                  width * 0.5),
                                                      child: Text(
                                                        'Order placed at : ${DateFormat('dd-MM-yyyy hh:mm a').format(order.createdAt)} ',
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize: 16,
                                                        ),
                                                      ),
                                                    ),
                                                    Container(
                                                      constraints:
                                                          BoxConstraints(
                                                              maxWidth:
                                                                  width * 0.54),
                                                      child: Text(
                                                        order.delivered
                                                            ? 'Delivered: ${DateFormat('dd-MM-yyyy hh:mm a').format(order.updatedAt)}'
                                                            : 'Not delivered yet',
                                                        style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          fontSize:
                                                              order.delivered
                                                                  ? 16
                                                                  : 16,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                          Container(
                                            child: IconButton(
                                              padding: EdgeInsets.only(
                                                  right: width * 0.03),
                                              icon: Icon(
                                                Icons
                                                    .arrow_forward_ios_outlined,
                                                size: 20,
                                              ),
                                              onPressed: () {
                                                // Navigator.push(
                                                //   context,
                                                //   MaterialPageRoute(
                                                //     builder: (context) =>
                                                //         OrderDetails(
                                                //             order: order.product),
                                                //   ),
                                                // );
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        OrderStatus(
                                                            order: order),
                                                  ),
                                                ).then((value) {
                                                  if (value) {
                                                    setState(() {
                                                      order.delivered = true;
                                                      order.updatedAt =
                                                          DateTime.now();
                                                    });
                                                  }
                                                });
                                              },
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            })
                          ],
                        ),
                      ),
              ),
      ),
    );
  }
}
