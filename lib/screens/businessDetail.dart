import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import '../providers/auth.dart';
import '../widgets/shopImage_picker.dart';
import '../screens/welcome_screen.dart';

class BusinessDetail extends StatefulWidget {
  static const routeName = '/business';
  final String userId;
  BusinessDetail({this.userId});
  @override
  _BusinessDetailState createState() => _BusinessDetailState();
}

class _BusinessDetailState extends State<BusinessDetail> {
  final LocalStorage storage = new LocalStorage('QUICKSHOP_V');
  final _form = GlobalKey<FormState>();
  var _isLoading = false;
  var _ownerName = '';
  var _shopName = '';
  String _imageUrl;

  void _getImageUrl(String url) {
    _imageUrl = url;
  }

  void _saveForm() async {
    var isValid = _form.currentState.validate();
    if (!isValid) {
      return;
    }
    _form.currentState.save();

    setState(() {
      _isLoading = true;
    });
    final userDetails = await Provider.of<Auth>(context, listen: false)
        .insertShop(_ownerName, _shopName, _imageUrl);
    storage.setItem(
      'userDetails',
      json.encode({
        'userId': userDetails['_id'],
        'token': userDetails['token'],
        'formattedAddress': userDetails['location']['formattedAddress'],
        'zipcode': userDetails['location']['zipcode'],
        'email': userDetails['email'],
        'ownerName': userDetails['ownerName'],
        'shopName':userDetails['shopName'],
        'mobileNo':
            userDetails['mobileNo'] == null ? '' : userDetails['mobileNo'],
        'shopImageUrl': userDetails['shopImageUrl'],
      }),
    );
    setState(() {
      _isLoading = false;
    });

    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => WelcomeScreen(
          userId: widget.userId,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              padding: EdgeInsets.all(width * 0.03),
              margin: EdgeInsets.all(width * 0.05),
              child: Form(
                key: _form,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: ListView(
                        children: [
                          Text(
                            'Enter your business details',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 22),
                          ),
                          SizedBox(
                            height: width * 0.08,
                          ),
                          ShopImage(
                            _getImageUrl,
                            widget.userId,
                          ),
                          SizedBox(
                            height: width * 0.05,
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              labelText: 'Shop Owner Name',
                              labelStyle: TextStyle(fontSize: 20),
                            ),
                            style: TextStyle(fontSize: 20),
                            textInputAction: TextInputAction.next,
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter name of shop owner';
                              }
                              if (value.length < 4) {
                                return 'Owner name should contain alteast 4 character';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              return _ownerName = value;
                            },
                          ),
                          TextFormField(
                            decoration: InputDecoration(
                              labelText: 'Shop Name',
                              labelStyle: TextStyle(fontSize: 20),
                            ),
                            style: TextStyle(fontSize: 20),
                            validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter shop name';
                              }
                              if (value.length < 4) {
                                return 'Shop name should contain alteast 4 character';
                              }
                              return null;
                            },
                            onSaved: (value) {
                              return _shopName = value;
                            },
                            onFieldSubmitted: (value) {
                              _saveForm();
                            },
                          ),
                          SizedBox(
                            height: width * 0.08,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: width * 0.4,
                      height: width * 0.12,
                      // alignment: Alignment.bottomCenter,
                      child: RaisedButton(
                        color: Theme.of(context).primaryColor,
                        onPressed: _saveForm,
                        child: Text(
                          'Submit',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 24),
                        ),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
    );
  }
}
