import 'package:flutter/material.dart';
import '../screens/startUp_screen.dart';

class WelcomeScreen extends StatelessWidget {
  static const routeName = '/welcome';
  final String userId;
  WelcomeScreen({this.userId});

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.blue[800],
      body: Column(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircleAvatar(
                  backgroundColor: Colors.white,
                  radius: 35,
                  child: Container(
                    alignment: Alignment.center,
                    child: Icon(
                      Icons.done,
                      size: 50,
                    ),
                  ),
                ),
                SizedBox(
                  height: width * 0.08,
                ),
                Text(
                  'Store successfully created!',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                  ),
                ),
                SizedBox(
                  height: width * 0.01,
                ),
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: width * 0.04),
                  child: Text(
                    "Congratulations on setting up your new store. Now it's time to add your first product.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 17,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            margin: EdgeInsets.only(
              bottom: width * 0.08,
              left: width * 0.05,
              right: width * 0.05,
            ),
            height: width * 0.12,
            child: RaisedButton(
              color: Colors.white,
              onPressed: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => StartUpPage(
                      userId: userId,
                    ),
                  ),
                );
              },
              child: Text(
                'Add your first product',
                style: TextStyle(
                  color: Colors.blue[800],
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5),
              ),
            ),
          )
        ],
      ),
    );
  }
}
