import 'dart:convert';

import 'package:QuickshopVendors/screens/businessDetail.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import '../providers/location.dart';
import 'map_screen.dart';

class UserLocation extends StatefulWidget {
  static const routeName = '/user-location';
  final String userId;
  UserLocation({this.userId});

  @override
  _UserLocationState createState() => _UserLocationState();
}

class _UserLocationState extends State<UserLocation> {
  final LocalStorage storage = new LocalStorage('QUICKSHOP_V');

  bool _isLoading = false;
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Center(
          child: Text(
            'Choose your location',
            style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
                fontFamily: 'MarkaziText'),
          ),
        ),
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Container(
              alignment: Alignment.center,
              child: Column(
                children: [
                  SizedBox(height: height * 0.1),
                  Text(
                    'Provide your shop location ',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: width * 0.1),
                  Container(
                    width: 300,
                    child: FlatButton.icon(
                      icon: Icon(Icons.search),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MapScreen(
                                      userId: widget.userId,
                                    )));
                      },
                      label: Text(
                        'Search for address',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  Container(
                    width: width * 0.5,
                    child: Divider(
                      color: Colors.black,
                      height: 2,
                    ),
                  ),
                  SizedBox(height: width * 0.05),
                  Text(
                    'Or',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: width * 0.05),
                  Consumer<Location>(
                    builder: (ctx, loc, _) => Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Theme.of(context).primaryColor,
                          width: 1,
                        ),
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: FlatButton.icon(
                        textColor: Theme.of(context).primaryColor,
                        onPressed: () async {
                          setState(() {
                            _isLoading = true;
                          });
                          try {
                            final userDetails =
                                await loc.getLocation(null, widget.userId);

                            storage.setItem(
                              'userDetails',
                              json.encode({
                                'userId': widget.userId,
                                'formattedAddress': userDetails['location']
                                    ['formattedAddress'],
                                'zipcode': userDetails['location']['zipcode'],
                                'email': userDetails['email'],
                              }),
                            );
                            setState(() {
                              _isLoading = false;
                            });
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => BusinessDetail(
                                  userId: widget.userId,
                                ),
                              ),
                            );
                          } catch (err) {
                            setState(() {
                              _isLoading = false;
                            });
                          }
                        },
                        icon: Icon(Icons.location_on_rounded),
                        label: Text(
                          'Use my location',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 18),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
    );
  }
}
