import 'dart:convert';

import 'package:QuickshopVendors/screens/listOfProduct.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:localstorage/localstorage.dart';

import 'package:provider/provider.dart';
import '../providers/product.dart';
import '../widgets/product_item.dart';
import 'addProduct.dart';

class ListOfCategory extends StatefulWidget {
  final String userId;
  ListOfCategory({this.userId});
  static const routeName = '/list';

  @override
  _ListOfCategoryState createState() => _ListOfCategoryState();
}

class _ListOfCategoryState extends State<ListOfCategory> {
  final LocalStorage storage = new LocalStorage('QUICKSHOP_V');
  var _isLoading = false;
  Map<String, dynamic> userDetails;

  List categoryList = [
    {
      'title': "Fruits & Vegetables",
      'icon': 'assets/svgIcons/fruits.svg',
    },
    {
      'title': "Foodgrains,Oil & Masala",
      'icon': 'assets/svgIcons/grains.svg',
    },
    {
      'title': "Bakery,Cakes & Dairy",
      'icon': 'assets/svgIcons/dairy.svg',
    },
    {
      'title': "Beverages",
      'icon': 'assets/svgIcons/drinks.svg',
    },
    {
      'title': "Snacks",
      'icon': 'assets/svgIcons/snacks.svg',
    },
    {
      'title': "Beauty & Hygiene",
      'icon': 'assets/svgIcons/shampoo.svg',
    },
    {
      'title': "Cleaning & Household",
      'icon': 'assets/svgIcons/household.svg',
    },
    {
      'title': "Baby Care",
      'icon': 'assets/svgIcons/baby.svg',
    },
  ];

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  fetchData() async {
    setState(() {
      _isLoading = true;
    });
    await storage.ready;
    userDetails = json.decode(storage.getItem('userDetails'));
    Provider.of<Product>(context, listen: false)
        .fetchAndSetProduct(userDetails['userId']);
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        body: _isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : Consumer<Product>(
                builder: (ctx, prod, _) {
                  List<ProductModel> products = prod.products;
                  return Container(
                    padding: EdgeInsets.all(width * 0.03),
                    margin: EdgeInsets.only(bottom: width * 0.05),
                    height: height * 0.85,
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          ...categoryList.map((list) {
                            final List<ProductModel> product = products
                                .where((data) => data.category == list['title'])
                                .toList();
                            return GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => ListOfProduct(
                                      products: product,
                                      userId: userDetails['userId'],
                                      category: list['title'],
                                    ),
                                  ),
                                ).then(
                                  (value) async {
                                    await Provider.of<Product>(context,
                                            listen: false)
                                        .fetchAndSetProduct(
                                            userDetails['userId']);
                                  },
                                );
                              },
                              child: Card(
                                child: Container(
                                  margin: EdgeInsets.all(width * 0.02),
                                  padding: EdgeInsets.symmetric(
                                    vertical: width * 0.03,
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                        child: Row(
                                          children: [
                                            SvgPicture.asset(
                                              list['icon'],
                                              height: 30,
                                              width: 30,
                                            ),
                                            SizedBox(
                                              width: width * 0.07,
                                            ),
                                            Text(
                                              list['title'],
                                              style: TextStyle(fontSize: 20),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios_sharp,
                                        color: Theme.of(context).primaryColor,
                                        size: 18,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }),
                        ],
                      ),
                    ),
                  );
                },
              ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: Container(
          width: width * 0.7,
          height: width * 0.12,
          margin: EdgeInsets.only(bottom: width * 0.05),
          child: RaisedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AddProductScreen(
                    userId: userDetails['userId'],
                  ),
                ),
              );
            },
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
            color: Colors.orangeAccent[700],
            child: Text(
              'Add new product',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                color: Colors.white,
                fontSize: 22,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
