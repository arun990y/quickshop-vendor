import 'dart:convert';

import 'package:QuickshopVendors/screens/businessDetail.dart';
import 'package:flutter/material.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:search_map_place/search_map_place.dart';
import '../credential.dart';
import '../providers/location.dart';

class MapScreen extends StatefulWidget {
  final String userId;
  static const routeName = '/map';
  MapScreen({this.userId});
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  final LocalStorage storage = new LocalStorage('QUICKSHOP_V');

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios_rounded),
          color: Colors.black,
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: SafeArea(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Consumer<Location>(
                      builder: (ctx, loc, _) => Container(
                        width: double.infinity,
                        child: SearchMapPlaceWidget(
                          clearIcon: Icons.clear,
                          hasClearButton: true,
                          placeType: PlaceType.address,
                          placeholder: 'Enter the location',
                          apiKey: MAP_API_KEY,
                          onSelected: (Place place) async {
                            Geolocation geolocation = await place.geolocation;
                            setState(() {
                              _isLoading = true;
                            });
                            final userDetails = await loc.getLocation(
                                geolocation.coordinates, widget.userId);

                            storage.setItem(
                              'userDetails',
                              json.encode({
                                'userId': userDetails['user']['_id'],
                                'token': userDetails['token'],
                                'formattedAddress': userDetails['user']
                                    ['location']['formattedAddress'],
                                'zipcode': userDetails['user']['location']
                                    ['zipcode'],
                                'email': userDetails['user']['email'],
                              }),
                            );

                            setState(() {
                              _isLoading = false;
                            });
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => BusinessDetail(
                                  userId: widget.userId,
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
