import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'listOfCategory.dart';
import 'order_screen.dart';
import 'account_screen.dart';
import '../widgets/drawer.dart';

class StartUpPage extends StatefulWidget {
  static const routeName = '/startPage';
  final String userId;
  final int index;
  StartUpPage({
    this.userId,
    this.index,
  });

  @override
  _StartUpPageState createState() => _StartUpPageState();
}

class _StartUpPageState extends State<StartUpPage> {
  int _selectedPageIndex = 0;
  List<Map<String, Object>> _pages;
  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  showExitAlert(BuildContext context) {
    return showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text(
          'Do you want to Exit?',
          style: TextStyle(
            fontSize: 24,
            color: Theme.of(context).errorColor,
          ),
        ),
        actions: [
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'No',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 22,
              ),
            ),
          ),
          FlatButton(
            onPressed: () {
              SystemChannels.platform.invokeMethod('SystemNavigator.pop');
            },
            child: Text(
              'Yes',
              style: TextStyle(
                color: Theme.of(context).primaryColor,
                fontSize: 22,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<bool> _willPopCallback() async {
    showExitAlert(context);
    return true;
  }

  @override
  void initState() {
    if (widget.index != null) {
      _selectedPageIndex = widget.index;
    }
    _pages = [
      {
        'page': ListOfCategory(
          userId: widget.userId,
        ),
        'title': 'List of Category',
      },
      {
        'page': OrderScreen(),
        'title': 'Orders',
      },
      {
        'page': AccountScreen(),
        'title': 'Profile',
      },
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: Text(
              _pages[_selectedPageIndex]['title'],
              style: Theme.of(context).appBarTheme.textTheme.headline1,
            ),
          ),
          drawer: MainDrawer(),
          body: _pages[_selectedPageIndex]['page'],
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            onTap: _selectPage,
            elevation: 10,
            selectedItemColor: Colors.blue[800],
            currentIndex: _selectedPageIndex,
            unselectedItemColor: Colors.black54,
            backgroundColor: Colors.white,
            showSelectedLabels: true,
            selectedFontSize: 20,
            showUnselectedLabels: true,
            items: [
              BottomNavigationBarItem(
                //: Theme.of(context).primaryColor,
                icon: SvgPicture.asset(
                  'assets/svgIcons/list.svg',
                  height: 20,
                  width: 20,
                ),
                label: 'Products',
              ),
              BottomNavigationBarItem(
                //: Theme.of(context).primaryColor,
                icon: SvgPicture.asset(
                  'assets/svgIcons/order.svg',
                  height: 20,
                  width: 20,
                ),
                label: 'Orders',
              ),
              BottomNavigationBarItem(
                //: Theme.of(context).primaryColor,
                icon: SvgPicture.asset(
                  'assets/svgIcons/user.svg',
                  height: 20,
                  width: 20,
                ),
                label: 'Profile',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
