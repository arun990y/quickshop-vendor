import 'dart:convert';
import 'dart:io';

import 'package:QuickshopVendors/providers/auth.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:localstorage/localstorage.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AccountScreen extends StatefulWidget {
  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  final LocalStorage storage = new LocalStorage('QUICKSHOP_V');
  var ownerName = '';
  var shopName = '';
  var _emailText = '';
  var _mobileText = '';
  final _formKey = GlobalKey<FormState>();
  Account userDetails;
  String imageUrl = '';
  var userInfo;

  var pickedImage = '';
  bool _isLoading = false;

  var _pickedImage;

  void _imgFromCamera() async {
    final picker = ImagePicker();

    final pickedImage = await picker.getImage(
      source: ImageSource.camera,
    );
    if (pickedImage == null) {
      return;
    }
    final pickedImageFile = File(pickedImage.path);
    setState(() {
      _pickedImage = pickedImageFile;
    });

    final ref = FirebaseStorage.instance
        .ref()
        .child('user_image')
        .child('userImage_' + userInfo['userId'] + '.jpg');

    await ref.putFile(pickedImageFile);

    var image = await ref.getDownloadURL();
    setState(() {
      imageUrl = image;
    });
  }

  _imgFromGallery() async {
    final picker = ImagePicker();

    final pickedImage = await picker.getImage(
      source: ImageSource.gallery,
    );
    if (pickedImage == null) {
      return;
    }
    final pickedImageFile = File(pickedImage.path);
    setState(() {
      _pickedImage = pickedImageFile;
    });

    final ref = FirebaseStorage.instance
        .ref()
        .child('user_image')
        .child('userImage_' + userInfo['userId'] + '.jpg');

    await ref.putFile(pickedImageFile);

    var image = await ref.getDownloadURL();
    setState(() {
      imageUrl = image;
    });
    print(imageUrl);
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    fetchUser();
  }

  fetchUser() async {
    setState(() {
      _isLoading = true;
    });
    await storage.ready;
    userInfo = json.decode(storage.getItem('userDetails'));
    setState(() {
      _isLoading = false;
    });
  }

  void save() async {
    final isValid = _formKey.currentState.validate();
    if (!isValid) {
      return;
    }
    _formKey.currentState.save();

    setState(() {
      _isLoading = true;
    });
    final Account user =
        await Provider.of<Auth>(context, listen: false).updateUser(
      userInfo['userId'],
      ownerName,
      shopName,
      _mobileText,
      imageUrl == '' ? userInfo['shopImageUrl'] : imageUrl,
    );
    storage.setItem(
      'userDetails',
      json.encode({
        'userId': userInfo['userId'],
        'token': userInfo['token'],
        'formattedAddress': userInfo['formattedAddress'],
        'zipcode': userInfo['zipcode'],
        'email': userInfo['email'],
        'shopImageUrl': user.shopImage,
        'ownerName': user.ownerName,
        'shopName': user.shopName,
        'mobileNo': user.mobileNo,
      }),
    );
    setState(() {
      userInfo = json.decode(storage.getItem('userDetails'));
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Container(
        height: height,
        width: width,
        child: _isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                // physics: BouncingScrollPhysics(),
                child: Container(
                  width: width,
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 20.0),
                          child: Stack(
                            fit: StackFit.loose,
                            children: <Widget>[
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                      width: height * 0.25,
                                      height: height * 0.25,

                                      // decoration: BoxDecoration(
                                      //   shape: BoxShape.circle,
                                      //   image: DecorationImage(
                                      //     image: _pickedImage == null
                                      //         ? userInfo == null
                                      //             ? AssetImage(
                                      //                 'assets/images/6.jpg')
                                      //             : NetworkImage(
                                      //                 userInfo['shopImageUrl'],
                                      //               )
                                      //         : FileImage(_pickedImage),
                                      //     fit: BoxFit.cover,
                                      //   ),
                                      // ),
                                      child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(200),
                                        child: _pickedImage == null
                                            ? userInfo == null
                                                ? Image.asset(
                                                    'assets/images/6.jpg',
                                                    fit: BoxFit.fill,
                                                  )
                                                : FadeInImage(
                                                    placeholder: AssetImage(
                                                        'assets/images/6.jpg'),
                                                    image: NetworkImage(
                                                      userInfo['shopImageUrl'],
                                                    ),
                                                    fit: BoxFit.fill,
                                                  )
                                            : Image.file(
                                                _pickedImage,
                                                fit: BoxFit.fill,
                                              ),
                                      )),
                                ],
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                  top: width * 0.42,
                                  right: width * 0.3,
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    InkWell(
                                      onTap: () {
                                        _showPicker(context);
                                      },
                                      child: CircleAvatar(
                                        backgroundColor: Colors.blue,
                                        radius: 20.0,
                                        child: Icon(
                                          Icons.edit,
                                          color: Colors.white,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: width * 0.1),
                        Padding(
                            padding: EdgeInsets.only(
                              left: width * 0.07,
                              right: width * 0.03,
                              top: width * 0.0001,
                              bottom: width * 0.01,
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text(
                                      'Owner Name',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black87,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )),
                        Padding(
                          padding: EdgeInsets.only(
                            left: height * 0.03,
                            right: height * 0.03,
                            bottom: width * 0.03,
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Flexible(
                                child: TextFormField(
                                  initialValue: userInfo['ownerName'],
                                  style: TextStyle(fontSize: 19),
                                  decoration: InputDecoration(
                                    hintText: "Owner Name",
                                    hintStyle: TextStyle(
                                      fontSize: 16.0,
                                    ),
                                    border: OutlineInputBorder(),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please provide full name';
                                    }
                                    return null;
                                  },
                                  onSaved: (value) {
                                    return ownerName = value.trim();
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(
                              left: width * 0.07,
                              right: width * 0.03,
                              top: width * 0.0001,
                              bottom: width * 0.01,
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text(
                                      'Shop Name',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black87,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )),
                        Padding(
                          padding: EdgeInsets.only(
                            left: height * 0.03,
                            right: height * 0.03,
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Flexible(
                                child: TextFormField(
                                  initialValue: userInfo['shopName'],
                                  style: TextStyle(fontSize: 19),
                                  decoration: InputDecoration(
                                    hintText: "Shop Name",
                                    hintStyle: TextStyle(
                                      fontSize: 16.0,
                                    ),
                                    border: OutlineInputBorder(),
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please provide full name';
                                    }
                                    return null;
                                  },
                                  onSaved: (value) {
                                    return shopName = value.trim();
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(
                              left: width * 0.07,
                              right: width * 0.03,
                              top: width * 0.034,
                              bottom: width * 0.01,
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text(
                                      'Email',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black87,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )),
                        Padding(
                          padding: EdgeInsets.only(
                            left: height * 0.03,
                            right: height * 0.03,
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Flexible(
                                child: TextFormField(
                                  initialValue: userInfo['email'],
                                  style: TextStyle(fontSize: 19),
                                  decoration: InputDecoration(
                                    enabled: false,
                                    hintText: "Email",
                                    hintStyle: TextStyle(
                                      fontSize: 16.0,
                                    ),
                                    border: OutlineInputBorder(),
                                  ),
                                  keyboardType: TextInputType.emailAddress,
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please provide email address';
                                    }
                                    if (!EmailValidator.validate(value)) {
                                      return 'Please enter valid email address';
                                    }
                                    return null;
                                  },
                                  onSaved: (value) {
                                    return _emailText = value.trim();
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.only(
                              left: width * 0.07,
                              right: width * 0.03,
                              top: width * 0.034,
                              bottom: width * 0.01,
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Text(
                                      'Phone Number',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black87,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )),
                        Padding(
                          padding: EdgeInsets.only(
                            left: height * 0.03,
                            right: height * 0.03,
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Flexible(
                                child: TextFormField(
                                  initialValue: userInfo['mobileNo'] == ''
                                      ? ''
                                      : userInfo['mobileNo'],
                                  style: TextStyle(fontSize: 19),
                                  decoration: InputDecoration(
                                    hintText: "Phone Number",
                                    hintStyle: TextStyle(
                                      fontSize: 16.0,
                                    ),
                                    counterText: '',
                                    border: OutlineInputBorder(),
                                  ),
                                  keyboardType: TextInputType.number,
                                  maxLength: 10,
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter your phone number';
                                    }
                                    if (value.length < 10) {
                                      return 'Please enter valid phone number';
                                    }
                                    return null;
                                  },
                                  onSaved: (value) {
                                    return _mobileText = value.trim();
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: width * 0.1,
                        ),
                        Center(
                          child: Container(
                            width: width * 0.7,
                            height: width * 0.12,
                            margin: EdgeInsets.only(bottom: width * 0.08),
                            child: RaisedButton(
                              color: Colors.blue,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onPressed: () {
                                save();
                              },
                              textColor: Colors.white,
                              child: Text(
                                "Update",
                                style: TextStyle(fontSize: 22),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}
