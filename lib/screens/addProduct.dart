import 'package:QuickshopVendors/screens/listOfCategory.dart';
import 'package:QuickshopVendors/screens/startUp_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import '../providers/product.dart';

import '../widgets/product_image.dart';

class AddProductScreen extends StatefulWidget {
  static const routeName = '/edit-product';
  final String userId;
  final ProductModel product;
  AddProductScreen({
    this.userId,
    this.product,
  });
  @override
  _AddProductScreenState createState() => _AddProductScreenState();
}

class _AddProductScreenState extends State<AddProductScreen> {
  final _form = GlobalKey<FormState>();
  var _isLoading = false;

  List<String> category = [
    "Fruits & Vegetables",
    "Foodgrains,Oil & Masala",
    "Bakery,Cakes & Dairy",
    "Beverages",
    "Snacks",
    "Beauty & Hygiene",
    "Cleaning & Household",
    "Baby Care",
  ];

  List<String> unit = [
    'peice',
    'gram',
    'liter',
    'kg',
    'dozen',
  ];

  void _imageUrl(String url) {
    _productImageUrl = url;
  }

  var _editedProduct = ProductModel(
    productName: '',
    category: '',
    description: '',
    price: 0,
    imageUrl: '',
    id: null,
    unit: '',
    quantity: 0,
  );

  String _productCategory;
  String _unit;
  String _productImageUrl;
  bool imageEmpty = false;
  bool unitEmpty = false;

  var _isInit = true;

  var _initValue = {
    "productName": '',
    "category": '',
    "description": '',
    "price": '',
    "imageUrl": '',
    'unit': '',
    'quantity': ''
  };

  @override
  void didChangeDependencies() {
    if (_isInit) {
      _editedProduct = widget.product;
      if (_editedProduct != null) {
        _initValue = {
          'productName': _editedProduct.productName,
          'price': _editedProduct.price.toString(),
          'description': _editedProduct.description,
          'unit': _editedProduct.unit,
          'quantity': _editedProduct.quantity.toString(),
        };
        _productImageUrl = _editedProduct.imageUrl;
      } else {
        _editedProduct = ProductModel(
          productName: '',
          category: '',
          description: '',
          price: 0,
          imageUrl: '',
          id: null,
          unit: '',
          quantity: 0,
        );
      }
    }

    _isInit = false;
    super.didChangeDependencies();
  }

  Future<void> _saveForm() async {
    final isValid = _form.currentState.validate();
    if (!isValid || _productImageUrl == '' || _unit == null) {
      if (_unit == null) {
        setState(() {
          unitEmpty = true;
        });
      }
      if (_productImageUrl == '') {
        setState(() {
          imageEmpty = true;
        });
      }
      return;
    }
    _form.currentState.save();
    setState(() {
      _isLoading = true;
      unitEmpty = false;
      imageEmpty = false;
    });

    if (_editedProduct.id == null) {
      await Provider.of<Product>(context, listen: false).addProduct(
        _editedProduct,
        widget.userId,
      );
      setState(() {
        _isLoading = false;
      });
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => StartUpPage(),
        ),
      );
    } else {
      await Provider.of<Product>(context, listen: false).updateProduct(
        _editedProduct.id,
        _editedProduct,
        widget.userId,
      );
      setState(() {
        _isLoading = false;
      });
      Navigator.of(context).pop(_editedProduct);
    }
  }

  Future<bool> _willPopCallback() async {
    Navigator.of(context).pop(_editedProduct);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    _productCategory = _editedProduct.category == ""
        ? _productCategory != null
            ? _productCategory
            : null
        : _productCategory == null
            ? _editedProduct.category
            : _productCategory;

    _unit = _editedProduct.category == ""
        ? _unit != null
            ? _unit
            : null
        : _unit == null
            ? _editedProduct.unit
            : _unit;
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            title: Text(
              _editedProduct.id == null ? 'Add Product' : "Update Product",
              style: Theme.of(context).appBarTheme.textTheme.headline1,
            ),
            centerTitle: true,
            actions: [
              IconButton(
                icon: Icon(
                  Icons.save,
                  color: Colors.white,
                ),
                onPressed: _saveForm,
              )
            ],
          ),
          body: _isLoading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Padding(
                  padding: EdgeInsets.all(width * 0.04),
                  child: Form(
                    key: _form,
                    child: ListView(
                      children: [
                        ProductImage(
                          _imageUrl,
                          _editedProduct.imageUrl,
                          widget.userId,
                        ),
                        if (imageEmpty)
                          Padding(
                            padding: EdgeInsets.only(left: width * 0.015),
                            child: Text(
                              'Please choose product image',
                              style: TextStyle(
                                fontSize: 17,
                                color: Theme.of(context).errorColor,
                              ),
                            ),
                          ),
                        SizedBox(height: width * 0.05),
                        buildTextFormField(
                          initValue: _initValue['productName'],
                          text: 'Product Name',
                          type: TextInputAction.next,
                          context: context,
                          valid: (value) {
                            if (value.isEmpty) {
                              return 'Please enter a product name.';
                            }
                            return null;
                          },
                          save: (value) {
                            _editedProduct = ProductModel(
                              id: _editedProduct.id,
                              productName: value,
                              category: _productCategory,
                              description: _editedProduct.description,
                              price: _editedProduct.price,
                              imageUrl: _productImageUrl,
                              quantity: _editedProduct.quantity,
                              unit: _unit,
                            );
                          },
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                            top: width * 0.03,
                            bottom: width * 0.03,
                          ),
                          child: DropdownButton(
                            // elevation: 10,
                            value: _productCategory,
                            isExpanded: true,
                            iconSize: 25,
                            iconEnabledColor: Colors.black,
                            hint: Text(
                              'Select Product category',
                              style: TextStyle(fontSize: 20),
                            ),
                            items: category.map(
                              (item) {
                                return DropdownMenuItem(
                                  value: item,
                                  child: Text(
                                    item,
                                    style: TextStyle(
                                      fontSize: 20,
                                    ),
                                  ),
                                );
                              },
                            ).toList(),
                            onChanged: (value) {
                              setState(() {
                                _productCategory = value;
                              });
                            },
                          ),
                        ),
                        buildTextFormField(
                          initValue: _initValue['price'].toString(),
                          text: 'Price',
                          type: TextInputAction.next,
                          keyboardType: TextInputType.number,
                          valid: (value) {
                            if (value.isEmpty) {
                              return 'Please enter a price.';
                            }
                            if (double.tryParse(value) == null) {
                              return 'Please enter a valid number';
                            }
                            if (double.parse(value) <= 0) {
                              return 'Please enter a number greater than zero';
                            }
                            return null;
                          },
                          save: (value) {
                            _editedProduct = ProductModel(
                              id: _editedProduct.id,
                              productName: _editedProduct.productName,
                              category: _productCategory,
                              description: _editedProduct.description,
                              price: double.parse(value),
                              imageUrl: _productImageUrl,
                              quantity: _editedProduct.quantity,
                              unit: _unit,
                            );
                          },
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: width * 0.4,
                              // margin: EdgeInsets.symmetric(horizontal: 20),
                              child: DropdownButton(
                                // elevation: 10,
                                value: _unit,
                                isExpanded: true,
                                iconSize: 25,
                                iconEnabledColor: Colors.black,
                                hint: Text(
                                  'Unit',
                                  style: TextStyle(fontSize: 20),
                                ),
                                items: unit.map(
                                  (item) {
                                    return DropdownMenuItem(
                                      value: item,
                                      child: Text(
                                        item,
                                        style: TextStyle(
                                          fontSize: 20,
                                        ),
                                      ),
                                    );
                                  },
                                ).toList(),
                                onChanged: (value) {
                                  setState(() {
                                    _unit = value;
                                  });
                                },
                              ),
                            ),
                            Container(
                              width: width * 0.4,
                              child: buildTextFormField(
                                text: 'Quantity',
                                initValue: _initValue['quantity'],
                                context: context,
                                onField: (_) => _saveForm(),
                                keyboardType: TextInputType.number,
                                valid: (value) {
                                  if (value.isEmpty) {
                                    return 'Please enter quantity.';
                                  }
                                  if (value.length <= 0) {
                                    return 'Should be greater than 0.';
                                  }
                                  return null;
                                },
                                save: (value) {
                                  _editedProduct = ProductModel(
                                    id: _editedProduct.id,
                                    productName: _editedProduct.productName,
                                    category: _productCategory,
                                    description: _editedProduct.description,
                                    price: _editedProduct.price,
                                    imageUrl: _productImageUrl,
                                    quantity: int.parse(value),
                                    unit: _unit,
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                        if (unitEmpty)
                          Row(
                            children: [
                              Center(
                                child: Text(
                                  'Please choose unit',
                                  style: TextStyle(
                                    fontSize: 17,
                                    color: Theme.of(context).errorColor,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        buildTextFormField(
                          text: 'Product Description',
                          initValue: _initValue['description'],
                          type: TextInputAction.done,
                          context: context,
                          onField: (_) => _saveForm(),
                          keyboardType: TextInputType.multiline,
                          maxLine: 2,
                          valid: (value) {
                            if (value.isEmpty) {
                              return 'Please enter a product description.';
                            }
                            if (value.length < 10) {
                              return 'Should atleast contain 10 characters.';
                            }
                            return null;
                          },
                          save: (value) {
                            _editedProduct = ProductModel(
                              id: _editedProduct.id,
                              productName: _editedProduct.productName,
                              category: _productCategory,
                              description: value,
                              price: _editedProduct.price,
                              imageUrl: _productImageUrl,
                              quantity: _editedProduct.quantity,
                              unit: _unit,
                            );
                          },
                        ),
                        SizedBox(height: width * 0.05),
                        Center(
                          child: Container(
                            width: width * 0.7,
                            height: width * 0.12,
                            child: RaisedButton(
                              color: Colors.blue,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                              onPressed: () {
                                _saveForm();
                              },
                              textColor: Colors.white,
                              child: Text(
                                _editedProduct.id == null
                                    ? 'Add Product'
                                    : "Update",
                                style: TextStyle(fontSize: 22),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
        ),
      ),
    );
  }
}

Widget buildTextFormField({
  String text,
  TextInputAction type,
  Function onField,
  Function valid,
  var initValue,
  int maxLine,
  Function save,
  TextInputType keyboardType,
  BuildContext context,
}) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: 8.0),
    child: TextFormField(
      decoration: InputDecoration(
          labelText: text,
          labelStyle: TextStyle(fontSize: 22),
          contentPadding: EdgeInsets.all(0)),
      style: TextStyle(fontSize: 22),
      textInputAction: type,
      initialValue: initValue,
      keyboardType: keyboardType,
      maxLines: maxLine,
      onFieldSubmitted: onField,
      validator: valid,
      onSaved: save,
    ),
  );
}
