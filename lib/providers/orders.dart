import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import '../api.dart';
import '../helper/handler.dart';

class OrderModel {
  final product;
  final int amount;
  final String shopId;
  final String userId;
  final String orderId;
  final String shopName;
  final String shopImage;
  final DateTime createdAt;
  DateTime updatedAt;
  final String customerName;
  final String customerNumber;
  final String email;
  final String address;
  final latitude;
  final longitude;
  bool paymentStatus;
  bool packed;
  bool shipped;
  bool delivered;
  final int deliveryCharge;

  OrderModel({
    this.orderId,
    this.createdAt,
    this.shopName,
    this.updatedAt,
    this.shopImage,
    this.product,
    this.amount,
    this.shopId,
    this.userId,
    this.address,
    this.customerName,
    this.customerNumber,
    this.email,
    this.latitude,
    this.deliveryCharge,
    this.longitude,
    this.delivered,
    this.packed = false,
    this.paymentStatus = false,
    this.shipped = false,
  });
}

class Order with ChangeNotifier {
  List<OrderModel> _orders = [];

  List<OrderModel> get orders {
    return [..._orders];
  }

  fetchVendorOrder(
    String shopId,
  ) async {
    final url = '$domain${endPoint['fetchVendorOrder']}';
    var body = json.encode({
      "shopId": shopId,
    });

    final response = await http.post(
      url,
      body: body,
      headers: {
        "Content-Type": "application/json",
      },
    );

    final extractedData = json.decode(response.body);
    List<OrderModel> loadedOrder = [];
    if (extractedData['data'] == null) {
      return;
    }
    extractedData['data'].forEach((order) {
      loadedOrder.add(OrderModel(
        orderId: order['_id'],
        product: List.from(order['products']),
        amount: order['amount'],
        shopId: order['shopId'],
        userId: order['userId'],
        shopImage: order['shopImage'],
        shopName: order['shopName'],
        customerName: order['customerName'],
        customerNumber: order['customerNumber'],
        email: order['email'],
        address: order['customerAddress']['address'],
        latitude: order['customerAddress']['coordinates']['latitude'],
        longitude: order['customerAddress']['coordinates']['longitude'],
        createdAt: DateTime.parse(order['createdAt']),
        updatedAt: DateTime.parse(order['updatedAt']),
        delivered: order['delivered'],
        packed: order['packed'],
        paymentStatus: order['paymentStatus'],
        shipped: order['shipped'],
        deliveryCharge: order['deliveryCharge'],
      ));
    });
    _orders = List.from(loadedOrder);
    return loadedOrder;
  }

  updateOrder(body, status) async {
    final url = '$domain${endPoint['updateOrder']}';

    final response = await http.put(
      url,
      body: body,
      headers: {
        "Content-Type": "application/json",
      },
    );

    final extractedData = json.decode(response.body);

    if (extractedData['data'] == null) {
      return;
    }

    _orders.forEach((order) {
      if (order.orderId == json.decode(body)['orderId']) {
        if (status == 'packed') {
          order.packed = true;
          order.updatedAt = DateTime.now();
        } else if (status == 'shipped') {
          order.shipped = true;
          order.updatedAt = DateTime.now();
        } else if (status == 'delivered') {
          order.delivered = true;
          order.updatedAt = DateTime.now();
        } else if (status == 'paid') {
          order.paymentStatus = true;
          order.updatedAt = DateTime.now();
        }
      }
    });
    notifyListeners();
  }
}
