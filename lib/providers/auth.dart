import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../api.dart';
import '../helper/handler.dart';

class Account {
  final String ownerName;
  final String shopName;
  final String shopImage;
  final String email;
  final String mobileNo;

  Account({
    this.email,
    this.mobileNo,
    this.ownerName,
    this.shopImage,
    this.shopName,
  });
}

class Auth with ChangeNotifier {
  String _userId;
  String _token;
  var _zipcode;
  String _address;
  String _shopName;

  String get address {
    return _address;
  }

  String get zipcode {
    return _zipcode;
  }

  String get shopName {
    return _shopName;
  }

  String get userId {
    return _userId;
  }

  String get token {
    if (_token != null) {
      return _token;
    }
    return null;
  }

  bool get auth {
    return token != null;
  }

  void logout() async {
    _token = null;
    _userId = null;
    notifyListeners();
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  login(String email, String password) async {
    try {
      final url = '$domain${endPoint['login']}';

      var body =
          json.encode({"email": email, "password": password, "role": "shop"});

      final response = await http.post(
        url,
        body: body,
        headers: {"Content-Type": "application/json"},
      );

      final extractedData = json.decode(response.body);

      _token = extractedData['token'];
      _userId = extractedData['userId'];
      _zipcode = extractedData['zipcode'];
      _address = extractedData['address'];
      _shopName = extractedData['shopName'];
      final prefs = await SharedPreferences.getInstance();
      final userData = json.encode(
        {
          'token': _token,
          'userId': _userId,
          'address': _address,
          'zipcode': _zipcode,
          'shopName': _shopName
        },
      );
      prefs.setString('user', userData);
      notifyListeners();

      if (extractedData['error'] != null) {
        throw ErrorHandler(extractedData['error']);
      }
      return extractedData;
    } catch (err) {
      throw err;
    }
  }

  signUp(String email, String password) async {
    try {
      final url = '$domain${endPoint['register']}';

      var body =
          json.encode({"email": email, "password": password, "role": "shop"});

      final response = await http.post(
        url,
        body: body,
        headers: {"Content-Type": "application/json"},
      );

      final extractedData = json.decode(response.body);
      _token = extractedData['token'];
      _userId = extractedData['userId'];
      notifyListeners();
      if (extractedData['error'] != null) {
        throw ErrorHandler(extractedData['error']);
      }
    } catch (err) {
      throw err;
    }
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('user')) {
      return false;
    }
    final extractedData =
        json.decode(prefs.getString('user')) as Map<String, Object>;

    _token = extractedData['token'];
    _userId = extractedData['userId'];
    _address = extractedData['address'];
    _zipcode = extractedData['zipcode'];
    _shopName = extractedData['shopName'];
    notifyListeners();
    return true;
  }

  insertShop(String owner, String shop, String imageUrl) async {
    final url = '$domain${endPoint['insertShop']}';

    var body = json.encode({
      "ownerName": owner,
      "shopName": shop,
      "role": "shop",
      "shopImageUrl": imageUrl
    });

    final response = await http.put(
      url,
      body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer $_token"
      },
    );
    print(imageUrl);
    final extractedData = json.decode(response.body);
    return extractedData['data'];
  }

  fetchUser(userId) async {
    final url = '$domain${endPoint['fetchUser']}';
    var body = json.encode({
      "userId": userId,
    });

    final response = await http.post(
      url,
      body: body,
      headers: {
        "Content-Type": "application/json",
      },
    );

    final extractedData = json.decode(response.body);
    final user = Account(
      email: extractedData['data']['email'],
      mobileNo: extractedData['data']['mobileNo'],
      ownerName: extractedData['data']['ownerName'],
      shopImage: extractedData['data']['shopImageUrl'],
      shopName: extractedData['data']['shopName'],
    );
    return user;
  }

  updateUser(String userId, String ownerName, String shopName, String mobileNo,
      String shopImageUrl) async {
    final url = '$domain${endPoint['updateUser']}';
    var body = json.encode({
      "userId": userId,
      "ownerName": ownerName,
      "shopName": shopName,
      "mobileNo": mobileNo,
      "shopImageUrl": shopImageUrl
    });

    final response = await http.put(
      url,
      body: body,
      headers: {
        "Content-Type": "application/json",
      },
    );

    final extractedData = json.decode(response.body);
    final user = Account(
      email: extractedData['data']['email'],
      mobileNo: extractedData['data']['mobileNo'],
      ownerName: extractedData['data']['ownerName'],
      shopImage: extractedData['data']['shopImageUrl'],
      shopName: extractedData['data']['shopName'],
    );
    return user;
  }
}
