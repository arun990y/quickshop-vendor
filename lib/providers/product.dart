import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import '../api.dart';

class ProductModel {
  final String productName;
  final String id;
  final double price;
  final int quantity;
  final String unit;
  final String description;
  String category;
  String imageUrl;
  ProductModel({
    this.id,
    this.category,
    this.unit,
    this.quantity,
    this.description,
    this.imageUrl,
    this.productName,
    this.price,
  });
}

class Product with ChangeNotifier {
  List<ProductModel> _products = [];

  String userID;
  Product(this.userID);

  List<ProductModel> get products {
    return [..._products];
  }

  Future<void> addProduct(
    ProductModel product,
    String userId,
  ) async {
    final url = '$domain${endPoint['insertProduct']}$userId';

    try {
      final body = json.encode({
        'productName': product.productName,
        'description': product.description,
        'price': product.price,
        'imageUrl': product.imageUrl,
        'category': product.category,
        'unit': product.unit,
        'quantity': product.quantity,
      });

      final response = await http.put(
        url,
        body: body,
        headers: {"Content-Type": "application/json"},
      );

      final extractedData = json.decode(response.body);
      print(extractedData);

      fetchAndSetProduct(userId);
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  fetchAndSetProduct(String userId) async {
    try {
      final url = '$domain${endPoint['fetchProduct']}$userId';

      final response = await http.get(url);

      final extractedData = json.decode(response.body);
      List<ProductModel> loadedProduct = [];

      extractedData['data']['products'].forEach((data) {
        loadedProduct.add(ProductModel(
          productName: data['productName'],
          category: data['category'],
          description: data['description'],
          id: data['_id'],
          imageUrl: data['imageUrl'],
          price: data['price'].toDouble(),
          unit: data['unit'],
          quantity: data['quantity'] == null ? 0 : data['quantity'].toInt(),
        ));
      });
      _products = loadedProduct;
      notifyListeners();
      return loadedProduct;
    } catch (e) {
      print(e);
    }
  }

  Future<void> deleteProduct(String productId, String userId) async {
    try {
      final url = '$domain${endPoint['deleteProduct']}$userId/$productId';

      final response = await http.put(url);
      print(json.decode(response.body));
      _products.removeWhere((prod) => prod.id == productId);
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  Future<void> updateProduct(
    String productId,
    ProductModel updatedProd,
    String userId,
  ) async {
    try {
      final url = '$domain${endPoint['updateProduct']}$userId/$productId';

      final body = json.encode(
        {
          'productName': updatedProd.productName,
          'description': updatedProd.description,
          'price': updatedProd.price,
          'imageUrl': updatedProd.imageUrl,
          'category': updatedProd.category,
          '_id': productId,
          'unit': updatedProd.unit,
          'quantity': updatedProd.quantity,
        },
      );

      await http.put(
        url,
        body: body,
        headers: {"Content-Type": "application/json"},
      );

      fetchAndSetProduct(userId);
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  updateProd(String prodId, product) {
    _products.forEach((prod) {
      if (prod.id == prodId) {
        prod = product;
      }
    });
    notifyListeners();
  }

  ProductModel findById(String productId) {
    return _products.firstWhere((prod) => prod.id == productId);
  }
}
